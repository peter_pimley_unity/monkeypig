﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddItem : MonoBehaviour {

    public GameObject inventoryManagerObject;
    private InventoryManager inventoryManagerScript;

	void Awake () {
        inventoryManagerScript = inventoryManagerObject.GetComponent<InventoryManager>();
	}
	

	void Update () {
        if (Input.GetKeyDown("space"))
            {
            Debug.Log("space pressed");
            inventoryManagerScript.AddItem(InventoryManager.ItemTypes.Jellybeans, 1);
            }
	}
}