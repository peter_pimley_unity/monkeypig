﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour {

    public static InventoryManager instance = null;

    //Declare item types
    public enum ItemTypes {RocketPart1, RocketPart2, RocketPart3, RocketPart4, Jellybeans, Cupcakes, Tangerines, Count}
    //Add any I've missed...


    private int[] items = new int[Enum.GetValues(typeof(ItemTypes)).Length];


	void Awake () {

        //Singleton pattern to avoid duplicate inventory objects
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            AddItem(ItemTypes.Jellybeans, 1000);
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            AddItem(ItemTypes.Cupcakes, 1000);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            AddItem(ItemTypes.Tangerines, 1000);
        }
    }

    //public method to add items.
    public void AddItem(ItemTypes itemType, int amount)
    {
        Debug.Log($"AddItem {itemType}: {amount}");
        items[(int)itemType] = items[(int)itemType] + amount;

    }

    public bool HasItems(ItemTypes itemType, int amount)
	{
		return items[(int)itemType] >= amount;
    }

    public int GetAmount(ItemTypes itemType)
    {
        return items[(int)itemType];
    }
}