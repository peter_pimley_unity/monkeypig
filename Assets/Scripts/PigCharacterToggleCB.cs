﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigCharacterToggleCB : MonoBehaviour {


    [SerializeField]
    private CharacterToggle m_toggle;

    [SerializeField]
    private CrateCarrier m_carrier;

    [SerializeField]
    private Rigidbody m_rb;

    [SerializeField]
    private Renderer [] m_renderers;

    void Awake ()
    {

        CharacterToggle.ToggleAction away = delegate (CharacterToggle other)
        {
            Debug.LogFormat("Pig toggle away, dropping everything.");
            m_carrier.DropCarried();
            foreach (Renderer r in m_renderers)
                r.enabled = false;
        };

        m_toggle.TogglingTo += away;


        m_toggle.ToggledFrom += delegate (CharacterToggle other)
        {
            Debug.LogFormat("Pig toggle to. Setting position from {0}", other.name);
            Transform otherT = other.transform;
            m_rb.transform.position = (otherT.position + (otherT.forward * 1));
            foreach (Renderer r in m_renderers)
                r.enabled = true;
        };

        Debug.LogFormat("PigCTCB awake.");
        away(null);
    }
}
