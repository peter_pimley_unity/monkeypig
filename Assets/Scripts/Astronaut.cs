﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astronaut : NPC
{
    public void TellPlayerAboutGuardian(ProgressionSystem.Guardian _guardian, ProgressionSystem.RocketPart _rocketPart)
    {
        Debug.Log("Tell player about RocketPart  " + _rocketPart + " at Guardian" + _guardian);
    }
}
