﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour {



	void OnTriggerEnter (Collider other)
	{
		Collectable c = other.GetComponent<Collectable>();
		if (c == null)
			return;

		c.NotifyCollected ();

		var im = InventoryManager.instance;
        Debug.Assert(im != null);
		im.AddItem (c.ItemType, 1);
	}
}
