﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Cinemachine;

public class ProgressionSystem : MonoBehaviour
{
    public enum Guardian
    {
        None,
        Cat,
        Centaur,
        Turtle,
        Count
    }

    public enum QuestProgress
    {
        None,
        Available,
        Blocked,
        InProgress,
        Finished,
        Count
    }

    public enum RocketPart
    {
        None,
        Bottom,
        Mid,
        Top,
        Count
    }

    public enum RocketProgress
    {
        None,
        Destroyed,
        OnePart,
        TwoParts,
        Complete,
        Count
    }

    public enum Trail
    {
        None,
        Jellybeans,
        Cupcakes,
        Tangerines,
        Count
    }

    public enum Cave
    {
        None,
        Cave1,
        Cave2,
        Cave3,
        Count
    }

    public enum CutScenes
    {
        Intro,
        Completion,
        Count
    }

    private static readonly Dictionary<Guardian, Trail> ms_GuardianToTrail = new Dictionary<Guardian, Trail>
    {
        { Guardian.Cat, Trail.Jellybeans},
        { Guardian.Centaur, Trail.Cupcakes},
        { Guardian.Turtle, Trail.Tangerines}
    };

    public static Trail GetGuradianTrail(Guardian _guardian)
    {
        return ms_GuardianToTrail.ContainsKey(_guardian)
            ? ms_GuardianToTrail[_guardian]
            : Trail.None;
    }

    private static readonly Dictionary<Guardian, RocketPart> ms_GuardianToRocketPart = new Dictionary<Guardian, RocketPart>
    {
        { Guardian.Cat, RocketPart.Bottom},
        { Guardian.Centaur, RocketPart.Mid},
        { Guardian.Turtle, RocketPart.Top}
    };

    public static RocketPart GetQuestReward(Guardian _guardian)
    {
        return ms_GuardianToRocketPart.ContainsKey(_guardian)
            ? ms_GuardianToRocketPart[_guardian]
            : RocketPart.None;
    }

    private static readonly Dictionary<RocketProgress, RocketPart> ms_RocketProgressRequirement = new Dictionary<RocketProgress, RocketPart>
    {
        { RocketProgress.Destroyed, RocketPart.Bottom},
        { RocketProgress.OnePart, RocketPart.Mid},
        { RocketProgress.TwoParts, RocketPart.Top}
    };

    public static RocketPart GetRequiredRocketPart(RocketProgress _rocketProgress)
    {
        return ms_RocketProgressRequirement.ContainsKey(_rocketProgress)
            ? ms_RocketProgressRequirement[_rocketProgress]
            : RocketPart.None;
    }

    public static Guardian GetGuardianForRocketProgress(RocketProgress _rocketProgress)
    {
        RocketPart requiredPart = GetRequiredRocketPart(_rocketProgress);
        return ms_GuardianToRocketPart.Where(x => x.Value == requiredPart).Select(x => x.Key).FirstOrDefault();
    }


    [SerializeField]
    private Astronaut m_astronaut;
    [SerializeField]
    private Rocket m_rocket;
    [SerializeField]
    private List<NPC> m_NPCs;
    [SerializeField]
    private CinemachineVirtualCamera m_rocketLaunchCamera;

    [SerializeField]
    private Guardian m_initialGuardian = Guardian.Cat;

    public Guardian CurrentGuardian { get; private set; } = Guardian.None;
    public Trail CurrenTrail { get; private set; } = Trail.None;

    private static ProgressionSystem ms_instance;
    public static ProgressionSystem Instance => ms_instance;

    private void Awake()
    {
        if (ms_instance == null)
        {
            ms_instance = this;
        }

        m_rocket.OnRocketProgressChanged += RocketProgressChangedHandler;
        m_rocket.OnRocketLaunchEnded += RocketLaunchEndedHandler;
    }

    private void RocketLaunchEndedHandler()
    {
        m_rocketLaunchCamera.Priority = 13;
        UIManager.Instance.TransitionTo(UIManager.View.Ending);
    }

    private void OnDestroy()
    {
        ms_instance = null;

        m_rocket.OnRocketProgressChanged -= RocketProgressChangedHandler;
        m_rocket.OnRocketProgressChanged += RocketProgressChangedHandler;
    }

    private void OnGuardianRevealed(Guardian _guardian)
    {
        CurrentGuardian = m_initialGuardian;
        GetGuradianNpc(CurrentGuardian).QuestProgress = QuestProgress.Available;
    }

    public void OnFirstGuardianRevealed()
    {
        OnGuardianRevealed(m_initialGuardian);
        Debug.Log("OnFirstGuardianRevealed: " + m_initialGuardian);
    }

	public void OnNextGuardianRevealed()
	{
	    CurrentGuardian = CurrentGuardian.Next();
	    GetGuradianNpc(CurrentGuardian).QuestProgress = QuestProgress.Available;

        m_rocket.Repair();

        Debug.Log("OnNextGuardianRevealed: " + CurrentGuardian);
    }

    public void OnGuardianQuestStarted(NPC npc)
	{
	    npc.QuestProgress = QuestProgress.InProgress;
	    CurrenTrail = GetGuradianTrail(npc.Guradian);
        npc.StartQuest();
	    Debug.Log($"OnGuardianQuestStarted: Guardian: {npc.Guradian} | Progress: {npc.QuestProgress} | Trail: {CurrenTrail}");
    }

	public void OnGuardianQuestFinished(NPC npc)
	{
	    npc.QuestProgress = QuestProgress.Finished;
        CurrenTrail = Trail.None;
        InventoryManager.instance.AddItem(GetQuestReward(npc.Guradian).ConvertToInventoryType(), 1);
	    Debug.Log($"OnGuardianQuestFinished: Guardian: {npc.Guradian} | Progress: {npc.QuestProgress} | Trail: {CurrenTrail}");
    }

    public NPC GetGuradianNpc(Guardian _guardian)
    {
        return m_NPCs.FirstOrDefault(x => x.Guradian == _guardian);
    }

    public ProgressionSystem.RocketProgress GetRocketProgress()
    {
        return m_rocket.Progress;
    }

    public event Action<Cave> OnRevealCave;

    private void RocketProgressChangedHandler(RocketProgress _rocketProgress)
    {
        Guardian nextGuardian = GetGuardianForRocketProgress(_rocketProgress);
        RocketPart nextRocketPart = GetQuestReward(nextGuardian);
        if (nextGuardian != Guardian.None)
        {
            m_astronaut.TellPlayerAboutGuardian(nextGuardian, nextRocketPart);
        }
    }

    [ContextMenu("LaunchRocket")]
    public void LaunchRocket()
    {
        m_rocket.Launch();
        m_rocketLaunchCamera.Priority = 13;
    }
}
