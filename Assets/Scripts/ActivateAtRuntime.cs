﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateAtRuntime : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        foreach (Transform t in transform)
            t.gameObject.SetActive(true);
        Destroy(this);
	}
	
}
