﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Destroyer : MonoBehaviour {


	private HashSet<Destroyable> m_currentDestroyables = new HashSet<Destroyable>();



	void OnTriggerEnter (Collider other)
	{
		Destroyable d = other.GetComponent<Destroyable>();
		if (d == null)
			return;
        Debug.LogFormat("{0} is eligible for destruction.", d.name);
		m_currentDestroyables.Add (d);
	}

	void OnTriggerExit (Collider other)
	{
		Destroyable d = other.GetComponent<Destroyable>();
		if (d == null)
			return;
        Debug.LogFormat("{0} is NO LONGER eligible for destruction.", d.name);
		m_currentDestroyables.Remove(d);
	}



    void Update ()
    {
        ListenForInput();
    }


    private void ListenForInput ()
    {
        bool down = Input.GetButtonDown("Break");
        if (!down)
            return;

        foreach (Destroyable d in m_currentDestroyables) {
            if (d == null)
                continue;
            d.BeDestroyedNowPlease();
        }
    }
}
