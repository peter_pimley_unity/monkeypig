﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour {

    [SerializeField]
    private ProgressionSystem.Guardian m_guradian;

    [SerializeField]
	private InventoryManager.ItemTypes m_requiredItemType;

	[SerializeField]
	private int m_numRequired;

	[SerializeField]
	public string m_dialogTreeName;

	[SerializeField]
	private AudioSource m_speechAudioSource;

	public ProgressionSystem.QuestProgress QuestProgress {get; set;}

    public ProgressionSystem.Guardian Guradian
    {
        get { return m_guradian; }
    }

	void Awake ()
	{
		QuestProgress = ProgressionSystem.QuestProgress.Blocked;
	}

	public void StartQuest ()
	{
        Collectable.ActivateAllOfType(m_requiredItemType);
	}

	public void Talk ()
	{
		DialogController.Instance.StartDialog (m_dialogTreeName, this);
	}

	public void PlayTalkAudio()
	{
		if (m_speechAudioSource != null)
		{
			m_speechAudioSource.Play();
		}
	}

	public bool PlayerHasRequiredItems()
	{
		return InventoryManager.instance.HasItems(m_requiredItemType, m_numRequired);
	}

    public int NumRemaining
    {
        get
        {
            return Mathf.Max(0, m_numRequired - InventoryManager.instance.GetAmount(m_requiredItemType));
        }
    }
}
