﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate : MonoBehaviour {


    [SerializeField]
    private InventoryManager.ItemTypes m_type;

    public InventoryManager.ItemTypes ItemType { get { return m_type; }}



    [SerializeField]
    private int m_capacity;

    public int Capacity { get { return m_capacity; }}


    [SerializeField]
    private GameObject m_explosionPrefab;

    public bool IsCarried { get; private set; }


    public void NotifyCarried ()
    {
        IsCarried = true;
    }

    public void NotifyDropped ()
    {
        IsCarried = false;
    }


    public void Start ()
    {
        Destroyable d = GetComponent<Destroyable>();
        if (d == null)
            return;

        d.DestroyRequested += GrantItems;
    }

    private void GrantItems ()
    {
        Debug.LogFormat("Crate destroyed, granting {0} of {1}", m_capacity, m_type);
        InventoryManager.instance.AddItem(m_type, m_capacity);
    }


    void OnDestroy ()
    {
        LeaveExplosion();
    }

    private void LeaveExplosion ()
    {
        GameObject prefab = m_explosionPrefab;
        if (prefab == null)
            return;
        GameObject o = Instantiate(prefab, transform.position, transform.rotation);

    }
}
