﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerMovement : MonoBehaviour {

	public float fuel = 100.0f;
	public float fuelUsage = 0.05f;
	public float fuelRefresh = 0.25f;

	public float jumpValue, walkSpeed;
	public string forwardAxis, horizontalAxis, flyButton;	

	public Animator frontAnim, backAnim;

	public Camera mainCamera;

	public bool canMove;

	Vector3 cameraRight;
	Vector3 cameraForward;

	public CinemachineFreeLook myCamera;

	private Rigidbody rigid;

	Vector3 newMovement = Vector3.zero;

	public AudioSource flyingAudioSource;

	public bool canFly = true;
	public bool isFlying { get; private set; }
	private bool isPlayingFlyingAudio;

	void Awake()
	{
		Cursor.lockState = CursorLockMode.Locked;
		rigid = GetComponent<Rigidbody>();
	}

	void FixedUpdate()
	{
		isFlying = false;

		if (IsAllowedToMove)
        {
            cameraRight = mainCamera.transform.right;
            cameraForward = mainCamera.transform.forward;
            cameraRight.y = 0;
            cameraForward.y = 0;
            cameraRight.Normalize();
            cameraForward.Normalize();
            Move();
            Jump();
            Rotate();
		}

		if (flyingAudioSource != null)
		{
			if (isFlying && !isPlayingFlyingAudio)
			{
				flyingAudioSource.Play();
				isPlayingFlyingAudio = true;
			}
			else if (!isFlying && isPlayingFlyingAudio)
			{
				flyingAudioSource.Stop();

				isPlayingFlyingAudio = false;
			}
		}
	}

	void Move()
	{
		if(Input.GetAxis(horizontalAxis) != 0 || Input.GetAxis(forwardAxis) != 0)
		{
			float hor = Input.GetAxis(horizontalAxis);
			float forward = Input.GetAxis(forwardAxis);

			newMovement = (cameraForward * forward + cameraRight * hor) * walkSpeed;
			newMovement.y = rigid.velocity.y;
			rigid.velocity = newMovement;

			if(forward < 0.0f)
			{
				frontAnim.SetBool("back", true);
				backAnim.SetBool("front", true);
				frontAnim.SetBool("running", true);
				backAnim.SetBool("running", true);
			}
			else if(forward > 0.0f)
			{
				frontAnim.SetBool("back", false);
				backAnim.SetBool("front", false);
				frontAnim.SetBool("running", true);
				backAnim.SetBool("running", true);
			}
			else
			{
				frontAnim.SetBool("back", false);
				backAnim.SetBool("front", false);
				frontAnim.SetBool("running", false);
				backAnim.SetBool("running", false);
			}
		}
	}

	void Jump()
	{
		if(canFly)
		{
			if(Input.GetButton(flyButton) && fuel > 0)
			{
				fuel -= fuelUsage;
				rigid.AddForce(Vector3.up * jumpValue);
				isFlying = true;
			}
			else if(fuel < 100.0f)
			{
				fuel += fuelRefresh;
			}
		}
	}

	void Rotate()
	{
		Vector3 rot = newMovement;
		rot.y = 0;

		transform.rotation = Quaternion.LookRotation(cameraForward);
    }

    private bool IsAllowedToMove
    {
        get
        {
            if (canMove)
            {
                return !UIManager.Instance.IsPaused;
            }

            return false;
        }
    }
}