﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

public class DialogDebug
{
	[MenuItem("MonkeyPig/Dialog/Spaceman")]
	public static void StartSpacemanDialog()
	{
		DialogController.Instance.StartDialog("Spaceman", null);
	}

	[MenuItem("MonkeyPig/Dialog/Cat")]
	public static void StartCatDialog()
	{
		NPC cat = GameObject.FindObjectsOfType<NPC>().FirstOrDefault(npc => npc.m_dialogTreeName == "Cat");
		DialogController.Instance.StartDialog("Cat", cat);
	}

	[MenuItem("MonkeyPig/Dialog/Horse")]
	public static void StartHorseDialog()
	{
		NPC horse = GameObject.FindObjectsOfType<NPC>().FirstOrDefault(npc => npc.m_dialogTreeName == "Horse?");
		DialogController.Instance.StartDialog("Horse?", horse);
	}

	[MenuItem("MonkeyPig/Dialog/Turtle")]
	public static void StartTurtleDialog()
	{
		NPC turtle = GameObject.FindObjectsOfType<NPC>().FirstOrDefault(npc => npc.m_dialogTreeName == "Turtle");
		DialogController.Instance.StartDialog("Turtle", turtle);
	}
}
