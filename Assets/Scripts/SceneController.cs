﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public struct AppSceneData
{
	public string Path { get; set; }
	public bool LoadInEditor { get; set; }
	public bool LoadOnGameStart { get; set; }
}

public static class SceneInfo
{
	public static IEnumerable<AppSceneData> GetAppScenes()
	{
		yield return new AppSceneData { Path = "Assets/Scenes/main.unity", LoadInEditor = true, LoadOnGameStart = true };
		yield return new AppSceneData { Path = "Assets/Scenes/Scene_Cave1.unity", LoadInEditor = true, LoadOnGameStart = true };
		yield return new AppSceneData { Path = "Assets/Scenes/Scene_Cave2.unity", LoadInEditor = true, LoadOnGameStart = true };
		yield return new AppSceneData { Path = "Assets/Scenes/Scene_Cave3.unity", LoadInEditor = true, LoadOnGameStart = true };
		yield return new AppSceneData { Path = "Assets/Scenes/Scene_Forest.unity", LoadInEditor = true, LoadOnGameStart = true };
		yield return new AppSceneData { Path = "Assets/Scenes/Scene_Dialog.unity", LoadInEditor = true, LoadOnGameStart = true };
		yield return new AppSceneData { Path = "Assets/Scenes/Scene_Render3DUI.unity", LoadInEditor = true, LoadOnGameStart = true };
	}
}

public class SceneController : MonoBehaviour
{
	private void Start()
	{
		foreach (AppSceneData data in SceneInfo.GetAppScenes())
		{
			if (data.LoadOnGameStart)
			{
				Scene scene = SceneManager.GetSceneByPath(data.Path);
				if (!scene.IsValid() || !scene.isLoaded)
				{
                    SceneManager.LoadScene(data.Path, LoadSceneMode.Additive);
                }
            }
		}
	}
}