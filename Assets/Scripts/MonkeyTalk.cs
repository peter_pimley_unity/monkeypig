﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MonkeyTalk : MonoBehaviour {




    private List<NPC> m_nearbyNpcs = new List<NPC>();



    void OnTriggerEnter (Collider other)
    {
        NPC n = other.GetComponent<NPC>();
        if (n == null)
            return;
        Debug.LogFormat("Can talk to {0}", n.name);
        m_nearbyNpcs.Add(n);
    }

    void OnTriggerExit (Collider other)
    {
        NPC n = other.GetComponent<NPC>();
        if (n == null)
            return;
        Debug.LogFormat("Can NO LONGER talk to {0}", n.name);
        m_nearbyNpcs.Remove(n);
    }



    void Update ()
    {
        ListenForInput();
    }


    private void ListenForInput ()
    {
        bool down = Input.GetButtonDown("Talk");
        if (!down)
            return;

        Debug.LogFormat("Talking to first of {0} NPCs", m_nearbyNpcs.Count());

        NPC n = m_nearbyNpcs.FirstOrDefault();
        if (n == null)
            return;

        n.Talk();
    }
}
