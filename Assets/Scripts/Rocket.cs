﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    public ProgressionSystem.RocketProgress Progress { get; private set; } = ProgressionSystem.RocketProgress.Destroyed;

    public event Action<ProgressionSystem.RocketProgress> OnRocketProgressChanged;
    public event Action OnRocketLaunchEnded;

	public AudioSource rocketTakeoffAudio;
	public AudioSource cheerAudio;

    public void Repair()
    {
        Progress = Progress.Next();
        OnRocketProgressChanged?.Invoke(Progress);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Repair();
        }
    }

    public void Launch()
    {
        GetComponent<Animator>().SetTrigger("LaunchRocket");
		if (rocketTakeoffAudio != null)
		{
			rocketTakeoffAudio.Play();
		}
	}

    public void RocketLaunchEndedHandler()
    {
        OnRocketLaunchEnded?.Invoke();
        Debug.Log("RocketLaunchEndedHandler");
		if (cheerAudio != null)
		{
			cheerAudio.Play();
		}
	}
}
