﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CrateCarrier : MonoBehaviour {


    private HashSet<Crate> m_nearbyCrates = new HashSet<Crate>();

    [SerializeField]
    private Transform m_carryPoint;


    public bool IsCurrentlyCarrying { get { return CarriedCrates.Count() > 0; } }


    public IEnumerable<Crate> CarriedCrates {
        get {
            return m_nearbyCrates.Where(x => x.IsCarried);
        }
    }

    void OnTriggerEnter (Collider other)
    {
        Crate crate = other.GetComponent<Crate>();
        if (crate == null)
            return;
        m_nearbyCrates.Add(crate);
        Debug.LogFormat("Crate \"{0}\" is eligable for collection.", other.name);
    }

    void OnTriggerExit (Collider other)
    {
        Crate crate = other.GetComponent<Crate>();
        if (crate == null)
            return;
        m_nearbyCrates.Remove(crate);

        Debug.LogFormat("Crate \"{0}\" is NO LONGER eligable for collection.", other.name);
    }



    public void GrabNearby ()
    {
        Debug.LogFormat("Grabbing");
        IEnumerable<Crate> crates = m_nearbyCrates.Where(x => x != null &&  !x.IsCarried);
        foreach (Crate c in crates)
            c.NotifyCarried(); 
    }


    public void DropCarried ()
    {
        Debug.LogFormat("Dropping");
        foreach (Crate c in m_nearbyCrates.Where(x => x != null &&  x.IsCarried))
            c.NotifyDropped();
    }


    void Update()
    {
        MoveCarried();
        ListenForInput();
    }



    private void ListenForInput ()
    {
        bool press = Input.GetButtonDown("Collect");
        if (!press)
            return;
        if (IsCurrentlyCarrying)
            DropCarried();
        else
            GrabNearby();

    }

    private void MoveCarried ()
    {
        foreach (Crate c in CarriedCrates)
        {
            Rigidbody rigidBody = c.GetComponent<Rigidbody>();
            rigidBody.position = m_carryPoint.position;
        } 
    }


}
