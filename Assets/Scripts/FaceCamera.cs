﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour {

	public Camera mainCamera;
	Vector3 cameraRight, cameraForward;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		LookAtCamera();
	}

	void LookAtCamera()
	{
		cameraRight = mainCamera.transform.right;
		cameraForward = mainCamera.transform.forward;
		cameraRight.y = 0;
		cameraForward.y = 0;
		cameraRight.Normalize();
		cameraForward.Normalize();

		transform.rotation = Quaternion.LookRotation(cameraForward);
	}
}
