﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaveEntrance : MonoBehaviour
{
    [SerializeField]
    private ProgressionSystem.Cave m_cave;

    private void Awake()
    {
        ProgressionSystem.Instance.OnRevealCave += RevealCaveHandler;
    }

    private void RevealCaveHandler(ProgressionSystem.Cave _cave)
    {
        if (_cave == m_cave)
        {
            RevealCave();
        }
    }

    private void OnDestroy()
    {
        ProgressionSystem.Instance.OnRevealCave -= RevealCaveHandler;
    }

    public void RevealCave()
    {
        gameObject.SetActive(false);
    }
}
