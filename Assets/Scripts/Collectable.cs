﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// An in-scene object that can be collected (Tangerine, etc)
public class Collectable : MonoBehaviour {



    static Dictionary<InventoryManager.ItemTypes, List<Collectable>>
    s_allByType = new Dictionary<InventoryManager.ItemTypes, List<Collectable>>();



	[SerializeField]
	private InventoryManager.ItemTypes m_type;

	public InventoryManager.ItemTypes ItemType {get {return m_type;}}


	private void Awake()
	{
        var type = m_type;
        List<Collectable> list = null;
        s_allByType.TryGetValue(type, out list);
        if (list == null) {
            list = new List<Collectable>();
            s_allByType.Add(type, list);
        }
        list.Add(this);
        gameObject.SetActive(false);
	}

	private void OnDestroy()
	{
        List<Collectable> list = null;
        s_allByType.TryGetValue(m_type, out list);
        if (list == null)
            return;
        list.Remove(this);
        if (list.Count == 0) {
            s_allByType.Remove(m_type);
        }
	}


    public static void ActivateAllOfType (InventoryManager.ItemTypes type)
    {
        List<Collectable> list = null;
        s_allByType.TryGetValue(type, out list);
        if (list == null)
            return;
        foreach (Collectable c in list)
            c.gameObject.SetActive(true);
    }

	// Called when collected by the Collector.
	public void NotifyCollected ()
	{
		Destroy(gameObject);
	}
}
