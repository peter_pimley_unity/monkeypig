﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraSwitcher : MonoBehaviour {

	public CinemachineFreeLook monkeyCam, pigCam;

	bool currentTargetMonkey = true;

	[ContextMenu("SwitchCamera")]
	public void SwitchCamera()
	{
		if(!currentTargetMonkey)
		{
			monkeyCam.m_Priority = 11;
			pigCam.m_Priority = 9;
			currentTargetMonkey = true;
		}
		else
		{
			monkeyCam.m_Priority = 9;
			pigCam.m_Priority = 11;
			currentTargetMonkey = false;
		}

	}
}
