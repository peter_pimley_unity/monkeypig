﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using System.Linq;

[DisallowMultipleComponent]
public class CharacterToggle : MonoBehaviour {



    private static List<CharacterToggle> s_instances = new List<CharacterToggle>();

    private static int s_lastToggle;

    public bool IsTheOne { get { return GetComponent<PlayerMovement>().canMove;}}


    // Emitted by the replacement character, passing in the previous one.
    public delegate void ToggleAction(CharacterToggle other);
    public event ToggleAction ToggledFrom;

    public event ToggleAction TogglingTo;

    void Awake ()
    {
        s_instances.Add(this);
        Debug.LogFormat("Added self: {0}, now {1}", GetInstanceID(), s_instances.Count());
    }

    void OnDestroy () {
        Debug.LogFormat("Remove me: {0}", GetInstanceID());
        s_instances.Remove(this);
    }

	// Update is called once per frame
	void Update () {
        ListenForInput();
	}


    private void ListenForInput ()
    {
        if (Time.frameCount == s_lastToggle)
            return; // Aready done this frame.


        if (!IsTheOne)
            return;
        bool down = Input.GetButtonDown("Deploy");
        if (!down)
            return;
        Debug.LogFormat("DEPLOY from {0}", name);
        var move = GetComponent<PlayerMovement>();


        Camera cam = Camera.main;

        Debug.LogFormat("Searching {0} instances.", s_instances.Count());
        CharacterToggle other = s_instances.First(x => x != this);

        Debug.LogFormat("Toggle from {0} to {1}", this.name, other.name);
        PlayerMovement otherMove = other.GetComponent<PlayerMovement>();
        otherMove.canMove = true;
        move.canMove = false;
        otherMove.mainCamera = cam;
        move.mainCamera = null;
        otherMove.myCamera.Priority = 11;
        move.myCamera.Priority = 9;


        PostProcessLayer postLayer = cam.GetComponent<PostProcessLayer>();
        if (postLayer != null) {
            postLayer.volumeTrigger = other.transform;
        }

        try {
            if (TogglingTo != null)
                TogglingTo(other);
        }
        catch (System.Exception x) {}

        try {
            var evnt = other.ToggledFrom;
            if (other.ToggledFrom != null)
                other.ToggledFrom(this);
        }
        catch (System.Exception x) {}


        s_lastToggle = Time.frameCount;

    }
}
