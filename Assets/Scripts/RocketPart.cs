﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketPart : MonoBehaviour
{
    [SerializeField]
    private ProgressionSystem.RocketProgress m_unlockRequirement;
    private Rocket m_rocket;

    private void Awake()
    {
        gameObject.SetActive(false);
        m_rocket = GetComponentInParent<Rocket>();
        m_rocket.OnRocketProgressChanged += RocketProgessChangedHandler;
    }

    private void OnDestroy()
    {
        m_rocket.OnRocketProgressChanged -= RocketProgessChangedHandler;
    }

    private void RocketProgessChangedHandler(ProgressionSystem.RocketProgress _rocketProgress)
    {
        if (_rocketProgress == m_unlockRequirement)
        {
            Reveal();
        }
    }

    private void Reveal()
    {
        gameObject.SetActive(true);
    }
}
