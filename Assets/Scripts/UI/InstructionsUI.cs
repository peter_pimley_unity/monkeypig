﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionsUI : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            OnClick_Confirm();
        }
    }

    public void OnClick_Confirm()
    {
        UIManager.Instance.OnConfirmClick();
        UIManager.Instance.TransitionTo(UIManager.View.Intro);
    }
}
