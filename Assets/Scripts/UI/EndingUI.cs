﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingUI : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            OnClick_Continue();
        }
    }

    public void OnClick_Continue()
    {
        UIManager.Instance.OnConfirmClick();
        UIManager.Instance.TransitionTo(UIManager.View.Frontend);
    }
}
