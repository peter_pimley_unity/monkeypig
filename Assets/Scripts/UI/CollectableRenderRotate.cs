﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableRenderRotate : MonoBehaviour
{
    private Dictionary<ProgressionSystem.Trail, GameObject> CollectableRoots = new Dictionary<ProgressionSystem.Trail, GameObject>();
    public GameObject CupcakePrefab;
    public GameObject JellybeanPrefab;
    public GameObject TangerinePrefab;
    public float AnglePerSecond = 180.0f;

    private void Awake()
    {
        if (CupcakePrefab != null)
        {
            var gameObjectInstance = Instantiate(CupcakePrefab, this.transform);
            CollectableRoots.Add(ProgressionSystem.Trail.Cupcakes, gameObjectInstance);
        }

        if (JellybeanPrefab != null)
        {
            var gameObjectInstance = Instantiate(JellybeanPrefab, this.transform);
            CollectableRoots.Add(ProgressionSystem.Trail.Jellybeans, gameObjectInstance);
        }

        if (TangerinePrefab != null)
        {
            var gameObjectInstance = Instantiate(TangerinePrefab, this.transform);
            CollectableRoots.Add(ProgressionSystem.Trail.Tangerines, gameObjectInstance);
        }

        foreach (var keyValuePair in CollectableRoots)
        {
            if (keyValuePair.Value != null)
            {
                SetLayerRecursively(keyValuePair.Value, this.gameObject.layer);
                keyValuePair.Value.gameObject.SetActive(false);
            }
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        //
        var progressionSystem = ProgressionSystem.Instance;
        if (progressionSystem != null)
        {
            SetCurrentCollectable(progressionSystem.CurrenTrail);
            //SetCurrentCollectable(ProgressionSystem.Trail.Tangerines);
        }

        //
        this.transform.localRotation = Quaternion.Euler(0.0f, AnglePerSecond * Time.deltaTime, 0.0f) * this.transform.localRotation;
    }

    public void SetCurrentCollectable(ProgressionSystem.Trail inCollectable)
    {
        foreach (var keyValuePair in CollectableRoots)
        {
            if (keyValuePair.Value != null)
            {
                keyValuePair.Value.gameObject.SetActive(false);
            }
        }

        if (CollectableRoots.ContainsKey(inCollectable))
        {
            var collectableGameObject = CollectableRoots[inCollectable];
            if (collectableGameObject != null)
            {
                CollectableRoots[inCollectable].gameObject.SetActive(true);
            }
        }
    }

    public void SetLayerRecursively(GameObject inGameObject, int layerNumber)
    {
        foreach (Transform trans in inGameObject.GetComponentsInChildren<Transform>(true))
        {
            trans.gameObject.layer = layerNumber;
        }
    }
}
