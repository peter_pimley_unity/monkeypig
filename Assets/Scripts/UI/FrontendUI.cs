﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontendUI : MonoBehaviour
{
    private enum ContentType
    {
        Menu,
        Credits,
    }

    private ContentType ActiveContent = ContentType.Menu;

    public Transform ContentParent;
    public GameObject MenuContent;
    public GameObject CreditsContent;

    private void Awake()
    {
        Debug.Assert(ContentParent != null);
        Debug.Assert(MenuContent != null);
        Debug.Assert(CreditsContent != null);
    }

    private void OnEnable()
    {
        ShowMenuContent();
    }

    private void Update()
    {
        switch (ActiveContent)
        {
            case ContentType.Credits:
                {
                    if (Input.GetKeyUp(KeyCode.Alpha1))
                    {
                        OnClick_Credits_Back();
                    }
                }
                break;

            case ContentType.Menu:
            default:
                {
                    if (Input.GetKeyUp(KeyCode.Alpha1))
                    {
                        OnClick_Menu_Start();
                    }

                    if (Input.GetKeyUp(KeyCode.Alpha2))
                    {
                        OnClick_Menu_Credits();
                    }

                    if (Input.GetKeyUp(KeyCode.Alpha3))
                    {
                        OnClick_Menu_Exit();
                    }

                    if (Input.GetKeyUp(KeyCode.Alpha1))
                    {
                        OnClick_Credits_Back();
                    }
                }
                break;
        }
    }

    public void OnClick_Menu_Start()
    {
        UIManager.Instance.OnConfirmClick();
        UIManager.Instance.TransitionTo(UIManager.View.Instructions);
    }

    public void OnClick_Menu_Credits()
    {
        //
        UIManager.Instance.OnConfirmClick();

        //
        HideAllContent();

        ActiveContent = ContentType.Credits;

        if (CreditsContent != null)
        {
            CreditsContent.gameObject.SetActive(true);
        }
    }

    public void OnClick_Menu_Exit()
    {
        UIManager.Instance.OnConfirmClick();
        Application.Quit();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }

    public void OnClick_Credits_Back()
    {
        UIManager.Instance.OnBackClick();
        ShowMenuContent();
    }

    private void HideAllContent()
    {
        if (ContentParent != null)
        {
            int childCount = ContentParent.transform.childCount;
            for (int childIndex = 0; childIndex < childCount; ++childIndex)
            {
                var child = ContentParent.GetChild(childIndex);
                child.gameObject.SetActive(false);
            }
        }
    }

    private void ShowMenuContent()
    {
        HideAllContent();

        ActiveContent = ContentType.Menu;

        if (MenuContent != null)
        {
            MenuContent.gameObject.SetActive(true);
        }
    }
}
