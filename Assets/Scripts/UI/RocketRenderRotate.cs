﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketRenderRotate : MonoBehaviour
{
    private Dictionary<ProgressionSystem.RocketProgress, GameObject> PartsRoots = new Dictionary<ProgressionSystem.RocketProgress, GameObject>();
    public GameObject OnePartPrefab;
    public GameObject TwoPartsPrefab;
    public GameObject CompletePrefab;
    public float AnglePerSecond = 180.0f;

    private void Awake()
    {
        if (OnePartPrefab != null)
        {
            var gameObjectInstance = Instantiate(OnePartPrefab, this.transform);
            PartsRoots.Add(ProgressionSystem.RocketProgress.OnePart, gameObjectInstance);
        }

        if (TwoPartsPrefab != null)
        {
            var gameObjectInstance = Instantiate(TwoPartsPrefab, this.transform);
            PartsRoots.Add(ProgressionSystem.RocketProgress.TwoParts, gameObjectInstance);
        }

        if (CompletePrefab != null)
        {
            var gameObjectInstance = Instantiate(CompletePrefab, this.transform);
            PartsRoots.Add(ProgressionSystem.RocketProgress.Complete, gameObjectInstance);
        }

        foreach (var keyValuePair in PartsRoots)
        {
            if (keyValuePair.Value != null)
            {
                SetLayerRecursively(keyValuePair.Value, this.gameObject.layer);
                keyValuePair.Value.gameObject.SetActive(false);
            }
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        //
        var progressionSystem = ProgressionSystem.Instance;
        if (progressionSystem != null)
        {
            SetCurrentRocketProgress(progressionSystem.GetRocketProgress());
        }

        //
        this.transform.localRotation = Quaternion.Euler(0.0f, AnglePerSecond * Time.deltaTime, 0.0f) * this.transform.localRotation;
    }

    public void SetCurrentRocketProgress(ProgressionSystem.RocketProgress inProgress)
    {
        foreach (var keyValuePair in PartsRoots)
        {
            if (keyValuePair.Value != null)
            {
                keyValuePair.Value.gameObject.SetActive(inProgress >= keyValuePair.Key);
            }
        }
    }

    public void SetLayerRecursively(GameObject inGameObject, int layerNumber)
    {
        foreach (Transform trans in inGameObject.GetComponentsInChildren<Transform>(true))
        {
            trans.gameObject.layer = layerNumber;
        }
    }
}
