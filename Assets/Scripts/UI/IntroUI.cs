﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroUI : MonoBehaviour
{
    [System.Serializable]
    public class SlideInfo
    {
        public Texture Slide;
        public string Text = "";
        public float TextTime = 0.0f;
    }

    private Coroutine SlideTextCoroutine;
    private int SlideIndex = -1;
    private bool TextSkipped = false;

    public SlideInfo[] Slides;
    public RawImage SlideImage;
    public Text SlideText;

    public void OnEnable()
    {
        NextSlide(true);
    }

    public void OnDisable()
    {
        if (SlideTextCoroutine != null)
        {
            StopCoroutine(SlideTextCoroutine);
            SlideTextCoroutine = null;
        }
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            OnClick_Continue();
        }

        if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            OnClick_Skip();
        }
    }

    private void NextSlide(bool inRestart = false)
    {
        TextSkipped = false;

        if (inRestart)
        {
            SlideIndex = 0;
        }
        else
        {
            ++SlideIndex;
        }

        if (SlideIndex < Slides.Length)
        {
            var slideInfo = Slides[SlideIndex];
            
            // Image
            if (SlideImage != null)
            {
                SlideImage.texture = slideInfo.Slide;
            }

            // Text
            if (slideInfo.TextTime > 0.0f)
            {
                SlideTextCoroutine = StartCoroutine(SlideText_Coroutine());
            }
            else
            {
                if (SlideText != null)
                {
                    SlideText.text = slideInfo.Text;
                }

                TextSkipped = true;
            }
        }
        else
        {
            OnClick_Skip();
        }
    }

    private IEnumerator SlideText_Coroutine()
    {
        var slideInfo = Slides[SlideIndex];

        // typing text
        float textStartTime = Time.realtimeSinceStartup;
        float textStartEndTime = textStartTime + slideInfo.TextTime;

        do
        {
            float progress = Mathf.Clamp01((Time.realtimeSinceStartup - textStartTime) / slideInfo.TextTime);

            if (SlideText != null)
            {
                SlideText.text = slideInfo.Text.Substring(0, (int)(progress * (float)slideInfo.Text.Length));
            }

            yield return new WaitForEndOfFrame();

            if (TextSkipped)
            {
                break;
            }
        }
        while (Time.realtimeSinceStartup < textStartEndTime);

        // full text
        if (SlideText != null)
        {
            SlideText.text = slideInfo.Text;
        }

        // flag as skipped
        TextSkipped = true;
    }

    public void OnClick_Continue()
    {
        if (TextSkipped)
        {
            UIManager.Instance.OnConfirmClick();
            NextSlide();
        }
        else
        {
            TextSkipped = true;
        }
    }

    public void OnClick_Skip()
    {
        UIManager.Instance.OnSkipClick();
        UIManager.Instance.TransitionTo(UIManager.View.InGame);
    }
}
