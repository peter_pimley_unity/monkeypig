﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    public GameObject RocketProgressRoot;

    public GameObject ObjectiveRoot;
    public GameObject ObjectiveIconRoot;
    public Text ObjectiveText;

    private void OnEnable()
    {
        UpdateProgression();
    }

    private void Update()
    {
        //
        UpdateProgression();

        //
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            OnClick_Pause();
        }

        /*if (Input.GetKeyUp(KeyCode.Alpha0))
        {
            OnClick_Ending();
        }*/
    }

    private void UpdateProgression()
    {
        var progressionSystem = ProgressionSystem.Instance;
        if (progressionSystem != null)
        {
            // Objective
            string objectiveText = "Talk to the astronaut";
            {
                if (ObjectiveIconRoot != null)
                {
                    ObjectiveIconRoot.gameObject.SetActive(false);
                }

                if (progressionSystem.CurrentGuardian != ProgressionSystem.Guardian.None)
                {
                    var guardian = progressionSystem.GetGuradianNpc(progressionSystem.CurrentGuardian);

                    if (guardian.QuestProgress == ProgressionSystem.QuestProgress.Available)
                    {
                        objectiveText = string.Format("Go see the {0}", guardian.m_dialogTreeName);
                    }
                    else if ((guardian.QuestProgress != ProgressionSystem.QuestProgress.Finished) && guardian.PlayerHasRequiredItems())
                    {
                        objectiveText = string.Format("Return to the {0}", guardian.m_dialogTreeName);
                    }
                    else if (guardian.QuestProgress == ProgressionSystem.QuestProgress.InProgress)
                    {
                        objectiveText = string.Format("Collect {0} {1}", guardian.NumRemaining, progressionSystem.CurrenTrail.ToString());

                        if (ObjectiveIconRoot != null)
                        {
                            ObjectiveIconRoot.gameObject.SetActive(true);
                        }
                    }
                }
            }
            SetObjectiveText(objectiveText);

            // Rocket
            if (RocketProgressRoot != null)
            {
                RocketProgressRoot.gameObject.SetActive(progressionSystem.GetRocketProgress() >= ProgressionSystem.RocketProgress.OnePart);
            }
        }
    }

    public void OnClick_Pause()
    {
        UIManager.Instance.TransitionTo(UIManager.View.Pause);
    }

    public void OnClick_Ending()
    {
        UIManager.Instance.TransitionTo(UIManager.View.Ending);
    }

    private void SetObjectiveText(string inText)
    {
        if (ObjectiveRoot != null)
        {
            ObjectiveRoot.gameObject.SetActive(!string.IsNullOrEmpty(inText));
        }

        if (ObjectiveText != null)
        {
            ObjectiveText.text = inText;
        }
    }
}
