﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public enum View
    {
        Frontend,
        Instructions,
        Intro,
        InGame,
        Ending,
        Pause,
    }

    [System.Serializable]
    public class ViewInfo
    {
        public View View;
        public GameObject Parent;
        public bool KeepActiveUI = false;
    }

    public static UIManager Instance { get; private set; }
    public ViewInfo[] Views;
    public View InitialView = View.Frontend;

    private void Awake()
    {
        Instance = this;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    private void Start()
    {
        TransitionTo(InitialView);
    }

    public void TransitionTo(View inView)
    {
        var viewInfo = GetView(inView);

        if (viewInfo != null)
        {
            if (!viewInfo.KeepActiveUI)
            {
                HideAllViews();
            }

            if (viewInfo.Parent != null)
            {
                viewInfo.Parent.gameObject.SetActive(true);
            }
        }
    }

    private void HideAllViews()
    {
        for (int viewIndex = 0; viewIndex < Views.Length; ++viewIndex)
        {
            if (Views[viewIndex].Parent != null)
            {
                Views[viewIndex].Parent.gameObject.SetActive(false);
            }
        }
    }

    private ViewInfo GetView(View inView)
    {
        ViewInfo viewInfo = null;

        for (int viewIndex = 0; viewIndex < Views.Length; ++viewIndex)
        {
            if (Views[viewIndex].View == inView)
            {
                viewInfo = Views[viewIndex];
                break;
            }
        }

        return viewInfo;
    }

    public bool IsViewActive(View inView)
    {
        var viewInfo = GetView(inView);
        bool isActive = false;

        if (viewInfo != null)
        {
            if (viewInfo.Parent != null)
            {
                isActive = viewInfo.Parent.activeSelf;
            }
        }

        return isActive;
    }

    public bool IsPaused
    {
        get
        {
            return !UIManager.Instance.IsViewActive(UIManager.View.InGame) || UIManager.Instance.IsViewActive(UIManager.View.Pause);
        }
    }

    public void OnConfirmClick()
    {
        // todo : confirm sound
    }

    public void OnBackClick()
    {
        // todo : back sound
    }

    public void OnSkipClick()
    {
        // todo : skip sound
    }
}
