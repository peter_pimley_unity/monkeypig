﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour
{
    private enum OptionType
    {
        None,
        Restart,
        Quit
    }

    private OptionType ActiveOption = OptionType.None;

    public GameObject ConfirmOptions;
    public Text ConfirmText;
    public string RestartConfirmMessage;
    public string QuitConfirmMessage;

    private void Awake()
    {
        Debug.Assert(ConfirmOptions != null);
    }

    private void Update()
    {
        switch (ActiveOption)
        {
            case OptionType.None:
                {
                    if (Input.GetKeyUp(KeyCode.Alpha1))
                    {
                        OnClick_Resume();
                    }

                    if (Input.GetKeyUp(KeyCode.Alpha2))
                    {
                        OnClick_Restart();
                    }

                    if (Input.GetKeyUp(KeyCode.Alpha3))
                    {
                        OnClick_Quit();
                    }
                }
                break;

            default:
                {
                    if (Input.GetKeyUp(KeyCode.Alpha1))
                    {
                        OnClick_ConfirmationNo();
                    }

                    if (Input.GetKeyUp(KeyCode.Alpha2))
                    {
                        OnClick_ConfirmationYes();
                    }
                }
                break;
        }
    }

    private void OnEnable()
    {
        HideConfirmOptions();
    }

    public void OnClick_Resume()
    {
        UIManager.Instance.OnConfirmClick();

        //
        this.gameObject.SetActive(false);

        // todo : resume

        UIManager.Instance.TransitionTo(UIManager.View.InGame);
    }

    public void OnClick_Restart()
    {
        UIManager.Instance.OnConfirmClick();
        ShowConfirmOptions(OptionType.Restart, RestartConfirmMessage);
    }

    public void OnClick_Quit()
    {
        UIManager.Instance.OnConfirmClick();
        ShowConfirmOptions(OptionType.Quit, QuitConfirmMessage);
    }

    public void OnClick_ConfirmationNo()
    {
        UIManager.Instance.OnBackClick();
        HideConfirmOptions();
    }

    public void OnClick_ConfirmationYes()
    {
        //
        UIManager.Instance.OnConfirmClick();

        //
        this.gameObject.SetActive(false);

        switch (ActiveOption)
        {
            case OptionType.Restart:
                {
                    // todo : restart
                    UIManager.Instance.TransitionTo(UIManager.View.Instructions);
                }
                break;

            case OptionType.Quit:
            default:
                {
                    // todo : quit to frontend
                    UIManager.Instance.TransitionTo(UIManager.View.Frontend);
                }
                break;
        }
    }

    private void ShowConfirmOptions(OptionType inType, string inText)
    {
        ActiveOption = inType;

        if (ConfirmText != null)
        {
            ConfirmText.text = inText;
        }

        if (ConfirmOptions != null)
        {
            ConfirmOptions.gameObject.SetActive(true);
        }
    }

    private void HideConfirmOptions()
    {
        if (ConfirmOptions != null)
        {
            ConfirmOptions.SetActive(false);
        }

        ActiveOption = OptionType.None;
    }
}
