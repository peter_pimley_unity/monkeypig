﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyable : MonoBehaviour {



    public event System.Action DestroyRequested;


	public void BeDestroyedNowPlease ()
	{
        if (DestroyRequested != null)
            DestroyRequested();
		Destroy (gameObject);
	}
}
