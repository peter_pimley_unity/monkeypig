﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DialogController : MonoBehaviour
{

	public static DialogController Instance {get; private set;}

	private List<DialogTree> dialogTrees = new List<DialogTree>();

	public DialogUI dialogUI;
	public NPC currentNPC { get; private set; }

	private void Start()
	{
		Instance = this;
		AddDialog_Spaceman();
		AddDialog_Cat();
		AddDialog_Horse();
		AddDialog_Turtle();
	}

	public void StartDialog(string treeName, NPC npc)
	{
		DialogTree tree = this.dialogTrees.FirstOrDefault(t => string.Compare(t.name, treeName, true) == 0);
		if (tree == null)
		{
			Debug.LogError("Failed to find DialogTree: " + treeName);
			return;
		}
		if (tree.pages.Count == 0)
		{
			Debug.LogError("DialogTree " + treeName + " has no pages");
			return;
		}
		this.currentNPC = npc;
		dialogUI.StartDialog(tree);
	}

	private void AddDialog_Spaceman()
	{
		var builder = DialogTreeBuilder.Start("Spaceman")
			.InitialPage("Initial")
				.Condition(() => ProgressionSystem.Instance.CurrentGuardian == ProgressionSystem.Guardian.None)
			.InitialPage("HasBasePart")
				.Condition(() => ProgressionSystem.Instance.CurrentGuardian == ProgressionSystem.Guardian.Cat
					&& ProgressionSystem.Instance.GetGuradianNpc(ProgressionSystem.Instance.CurrentGuardian).QuestProgress == ProgressionSystem.QuestProgress.Finished)
			.InitialPage("HasMiddlePart")
				.Condition(() => ProgressionSystem.Instance.CurrentGuardian == ProgressionSystem.Guardian.Centaur
					&& ProgressionSystem.Instance.GetGuradianNpc(ProgressionSystem.Instance.CurrentGuardian).QuestProgress == ProgressionSystem.QuestProgress.Finished)
			.InitialPage("HasTopPart")
				.Condition(() => ProgressionSystem.Instance.CurrentGuardian == ProgressionSystem.Guardian.Turtle
					&& ProgressionSystem.Instance.GetGuradianNpc(ProgressionSystem.Instance.CurrentGuardian).QuestProgress == ProgressionSystem.QuestProgress.Finished)
			.InitialPage("ComeBackWithPart")
				.Condition(() => ProgressionSystem.Instance.CurrentGuardian != ProgressionSystem.Guardian.None
					&& ProgressionSystem.Instance.GetGuradianNpc(ProgressionSystem.Instance.CurrentGuardian).QuestProgress != ProgressionSystem.QuestProgress.Finished)
			.InitialPage("...")
			.Page("Initial", "I crash landed on this planet and need to go home! Can you help?")
				.Inline("Sure, what can I do?", "When I crashed all the parts were thrown far and wide! You could go and find them for me?")
				.Inline("I can do that!", "Thank you so much! First you need to find the BASE of the Rocket.")
				.Option("Ok, I'll be back as soon as I can!")
					.Action(() => ProgressionSystem.Instance.OnFirstGuardianRevealed())
			.Page("HasBasePart", "Well done, you've found the BASE part. Now we need the MIDDLE of the Rocket.")
				.Option("Ok, I'll be back as soon as I can!")
					.Action(() => ProgressionSystem.Instance.OnNextGuardianRevealed())
			.Page("HasMiddlePart", "Well done, you've found the MIDDLE part. Now we need the TOP of the Rocket.")
				.Option("Ok, I'll be back as soon as I can!")
					.Action(() => ProgressionSystem.Instance.OnNextGuardianRevealed())
			.Page("HasTopPart", "Well done, you've found all the parts, now the Rocket is complete.")
				.Inline("Great, can we go to Space now?", "Yes, let's go")
		            .Action(() => ProgressionSystem.Instance.LaunchRocket())
            .Page("ComeBackWithPart", "We still need that part!")
				.Option("Ok")
			.Page("...", "...")
		;
		this.dialogTrees.Add(builder.tree);
	}

	private void AddDialog_Cat()
	{
		var builder = DialogTreeBuilder.Start("Cat")
			.InitialPage("Initial")
				.Condition(() => currentNPC.QuestProgress == ProgressionSystem.QuestProgress.Available)
			.InitialPage("InProgress_Enough")
				.Condition(() => currentNPC.QuestProgress == ProgressionSystem.QuestProgress.InProgress
					&& currentNPC.PlayerHasRequiredItems())
			.InitialPage("InProgress_NotEnough")
				.Condition(() => currentNPC.QuestProgress == ProgressionSystem.QuestProgress.InProgress)
			.InitialPage("...")
			.Page("Initial", "Zzzzzz")
				.Inline("Hello", "Zzzzzz")
				.Inline("HELLO!", "Yessss?")
				.Inline("Do you have the BASE rocket part?", "Might have.")
				.Inline("Can you fetch it for me please?", "Nah, can't be bothered.")
				.Inline("Oh please.", "If you bring me some magic sweets.")
				.Inline("Magic sweets? I've not seen any magic sweets.", "You have to want to see them.")
				.Inline("Ok, I want to see them.", "Then you shall SEE THEM!")
					.Action(() => ProgressionSystem.Instance.OnGuardianQuestStarted(currentNPC))
			.Page("InProgress_Enough", "Oh well done, I didn't think you'd bother.")
				.Inline("I am very determined.", "What does DETERMINED mean?")
				.Inline("It means I do what I say I'm going to do.", "Well then, here's the Rocket part!")
					.Action(() => ProgressionSystem.Instance.OnGuardianQuestFinished(currentNPC))
			.Page("InProgress_NotEnough")
				.Reply("I don't see any sweets.")
				.Reply("Where's the sweets?")
				.Reply("Gimme the sweets.")
				.RandomReply()
			.Page("...", "...")
		;
		this.dialogTrees.Add(builder.tree);
	}

	private void AddDialog_Horse()
	{
		var builder = DialogTreeBuilder.Start("Horse?")
			.InitialPage("Initial")
				.Condition(() => currentNPC.QuestProgress == ProgressionSystem.QuestProgress.Available)
			.InitialPage("InProgress_Enough")
				.Condition(() => currentNPC.QuestProgress == ProgressionSystem.QuestProgress.InProgress
					&& currentNPC.PlayerHasRequiredItems())
			.InitialPage("InProgress_NotEnough")
				.Condition(() => currentNPC.QuestProgress == ProgressionSystem.QuestProgress.InProgress)
			.InitialPage("...")
			.Page("Initial", "Zzzzzz")
				.Inline("Hello", "Zzzzzz")
				.Inline("HELLO!", "Yessss?")
				.Inline("Do you have the MIDDLE rocket part?", "Hmph.")
				.Inline("Can you fetch it for me please?", "No, I'm too tired.")
				.Inline("Oh please.", "If you bring me some cakes.")
				.Inline("Cakes? Where can I find cakes?", "I don't know, you'll have to look for them.")
				.Inline("Ok, I 'll go and find some.", "...")
					.Action(() => ProgressionSystem.Instance.OnGuardianQuestStarted(currentNPC))
			.Page("InProgress_Enough", "Oh thank you.")
				.Inline("You're welcome, can I have the rocket part now?.", "Neigh.")
				.Inline("Please?", "Fine, here's the Rocket part!")
					.Action(() => ProgressionSystem.Instance.OnGuardianQuestFinished(currentNPC))
			.Page("InProgress_NotEnough")
				.Reply("I don't see any cakes.")
				.Reply("Where's the cakes?")
				.Reply("Gimme the cakes.")
				.RandomReply()
			.Page("...", "...")
		;
		this.dialogTrees.Add(builder.tree);
	}

	private void AddDialog_Turtle()
	{
		var builder = DialogTreeBuilder.Start("Turtle")
			.InitialPage("Initial")
				.Condition(() => currentNPC.QuestProgress == ProgressionSystem.QuestProgress.Available)
			.InitialPage("InProgress_Enough")
				.Condition(() => currentNPC.QuestProgress == ProgressionSystem.QuestProgress.InProgress
					&& currentNPC.PlayerHasRequiredItems())
			.InitialPage("InProgress_NotEnough")
				.Condition(() => currentNPC.QuestProgress == ProgressionSystem.QuestProgress.InProgress)
			.InitialPage("...")
			.Page("Initial", "Zzzzzz")
				.Inline("Hello", "Zzzzzz")
				.Inline("HELLO!", "Yessss?")
				.Inline("Do you have the TOP rocket part?", "Yes.")
				.Inline("Can you fetch it for me please?", "Nah.")
				.Inline("Oh please.", "If you bring me some juicy fruit.")
				.Inline("Juicy Fruit? Ok, I know the drill by now.", "Off you go then.")
					.Action(() => ProgressionSystem.Instance.OnGuardianQuestStarted(currentNPC))
			.Page("InProgress_Enough", "Well done, that's a lot of fruit! Here's the Rocket part.")
				.Inline("Thank you, I'll get this back to the Rocket.", "Bye")
					.Action(() => ProgressionSystem.Instance.OnGuardianQuestFinished(currentNPC))
			.Page("InProgress_NotEnough")
				.Reply("I don't see any fruit.")
				.Reply("Where's the fruit?")
				.Reply("Gimme the fruit.")
				.RandomReply()
			.Page("...", "...")
		;
		this.dialogTrees.Add(builder.tree);
	}
}
