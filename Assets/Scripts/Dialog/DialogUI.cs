﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DialogUI : MonoBehaviour
{
	public UnityEngine.UI.Text lastOptionText;
	public UnityEngine.UI.Text replyText;
	public List<UnityEngine.UI.Text> optionsText = new List<UnityEngine.UI.Text>();

	private PlayerMovement playerMovement;

	private DialogTree dialogTree;
	private DialogPage currentPage;
	private DialogReply currentReply;
	private DialogOption lastOption;
	private List<DialogOption> currentOptions;
	
	public void StartDialog(DialogTree tree)
	{
		this.dialogTree = tree;
		this.lastOption = null;
		ShowDialogUI(true);
		SetDialogPage(dialogTree.GetInitialPage());
	}

	private void ShowDialogUI(bool visible)
	{
		this.gameObject.SetActive(visible);
		if (this.playerMovement == null)
		{
			this.playerMovement = GameObject.FindObjectOfType<PlayerMovement>();
		}
		if (this.playerMovement != null)
		{
			this.playerMovement.canMove = !visible;
		}
	}

	public void SetDialogPage(DialogPage page)
	{
		this.currentPage = page;
		this.currentReply = page.GetReply();
		this.currentOptions = page.GetOptions().ToList();
		UpdateDialogText();
		if (DialogController.Instance.currentNPC != null)
		{
			DialogController.Instance.currentNPC.PlayTalkAudio();
		}
	}

	private void UpdateDialogText()
	{
		if (this.currentPage == null)
		{
			Debug.LogError("No page set");
			return;
		}
		if (this.lastOptionText != null)
		{
			if (this.lastOption != null)
			{
				this.lastOptionText.enabled = true;
				this.lastOptionText.text = this.lastOption.text;
			}
			else
			{
				this.lastOptionText.enabled = false;
			}
		}
		if (this.replyText != null)
		{
			this.replyText.text = (currentReply != null) ? currentReply.text : "...";
		}
		for (int i = 0; i < optionsText.Count; ++i)
		{
			if (i < this.currentOptions.Count)
			{
				optionsText[i].text = (i + 1).ToString() + ": " + this.currentOptions[i].text;
				optionsText[i].enabled = true;
			}
			else
			{
				optionsText[i].enabled = false;
			}
		}
	}

	private void Update()
    {
        if (UIManager.Instance.IsPaused)
        {
            return;
        }

        for (int i = 0; i < this.currentOptions.Count; ++i)
		{
			if (Input.GetKeyDown(KeyCode.Alpha1 + i))
			{
				SelectDialogOption(this.currentOptions[i]);
			}
		}
	}

	private void SelectDialogOption(DialogOption option)
	{
		for (int i = 0; i < option.actions.Count; ++i)
		{
			var action = option.actions[i];
			action();
		}
		this.lastOption = option;
		if (option.next != null)
		{
			SetDialogPage(option.next);
		}
		else
		{
			ShowDialogUI(false);
		}
	}
}
