﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDialogConditionOwner
{
	List<System.Func<bool>> conditions { get; }
}

public class DialogInitialPage : IDialogConditionOwner
{
	public DialogPage page;
	public List<System.Func<bool>> conditions { get; } = new List<System.Func<bool>>();
}

public class DialogReply : IDialogConditionOwner
{
	public string text;
	public List<System.Func<bool>> conditions { get; } = new List<System.Func<bool>>();
}

public class DialogOption : IDialogConditionOwner
{
	public string text;
	public DialogPage next;
	public List<System.Func<bool>> conditions { get; } = new List<System.Func<bool>>();
	public List<System.Action> actions { get; } = new List<System.Action>();

	private static DialogOption _walkAway;
	public static DialogOption walkAway
	{
		get
		{
			if (_walkAway == null)
			{
				_walkAway = new DialogOption
				{
					text = "[End]",
					next = null
				};
			}
			return _walkAway;
		}
	}
}

public class DialogPage
{
	public string name;
	public List<DialogReply> replies = new List<DialogReply>();
	public List<DialogOption> options = new List<DialogOption>();
	public List<DialogPage> optionsFrom = new List<DialogPage>();
	public bool isEndPage = false;
	public bool randomReply = false;

	public DialogReply GetReply()
	{
		if (this.replies.Count == 0)
		{
			Debug.LogError("No replies specified on DialogPage " + name);
			return null;
		}
		List<DialogReply> validReplies = new List<DialogReply>(replies.Count);
		for (int i = 0; i < this.replies.Count; ++i)
		{
			var reply = this.replies[i];
			bool passesAllConditions = true;
			for (int j = 0; j < reply.conditions.Count; ++j)
			{
				var condition = reply.conditions[j];
				if (!condition())
				{
					passesAllConditions = false;
					break;
				}
			}
			if (passesAllConditions)
			{
				if (randomReply)
				{
					validReplies.Add(reply);
				}
				else
				{
					return reply;
				}
			}
		}
		if (validReplies.Count > 0)
		{
			int index = Random.Range(0, validReplies.Count);
			return validReplies[index];
		}
		return null;
	}

	public IEnumerable<DialogOption> GetOptions()
	{
		int returnedOptions = 0;
		for (int i = 0; i < this.options.Count; ++i)
		{
			var option = this.options[i];
			bool passesAllConditions = true;
			for (int j = 0; j < option.conditions.Count; ++j)
			{
				var condition = option.conditions[j];
				if (!condition())
				{
					passesAllConditions = false;
					break;
				}
			}
			if (passesAllConditions)
			{
				++returnedOptions;
				yield return option;
			}
		}
		if (returnedOptions == 0)
		{
			yield return DialogOption.walkAway;
		}
	}
}

public class DialogTree
{
	public string name;
	public List<DialogPage> pages = new List<DialogPage>();
	public List<DialogInitialPage> initialPages = new List<DialogInitialPage>();

	public DialogPage GetInitialPage()
	{
		DialogPage selectedPage = this.pages[0];
		for (int i = 0; i < this.initialPages.Count; ++i)
		{
			var initialPage = this.initialPages[i];
			bool passesAllConditions = true;
			for (int j = 0; j < initialPage.conditions.Count; ++j)
			{
				var condition = initialPage.conditions[j];
				if (!condition())
				{
					passesAllConditions = false;
					break;
				}
			}
			if (passesAllConditions)
			{
				selectedPage = initialPage.page;
				break;
			}
		}
		return selectedPage;
	}

	public DialogTree(string name)
	{
		this.name = name;
	}
}
