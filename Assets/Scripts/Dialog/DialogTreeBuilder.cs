﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DialogTreeBuilder
{
	public DialogTree tree;
	private DialogPage currentPage;
	private DialogReply currentReply;
	private DialogOption currentOption;
	private DialogInitialPage currentInitialPage;
	private IDialogConditionOwner currentConditionOwner;

	public static DialogTreeBuilder Start(string treeName)
	{
		DialogTreeBuilder builder = new DialogTreeBuilder();
		builder.tree = new DialogTree(treeName);
		return builder;
	}

	private DialogPage FindOrAddPage(string pageName)
	{
		DialogPage page = tree.pages.FirstOrDefault(p => string.Compare(p.name, pageName, true) == 0);
		if (page == null)
		{
			page = new DialogPage
			{
				name = pageName
			};
			tree.pages.Add(page);
		}
		return page;
	}

	private DialogReply FindOrAddReply(string text)
	{
		DialogReply reply = currentPage.replies.FirstOrDefault(r => string.Compare(r.text, text, true) == 0);
		if (reply == null)
		{
			reply = new DialogReply
			{
				text = text
			};
			currentPage.replies.Add(reply);
		}
		return reply;
	}

	private DialogOption FindOrAddOption(string text)
	{
		DialogOption option = currentPage.options.FirstOrDefault(o => string.Compare(o.text, text, true) == 0);
		if (option == null)
		{
			option = new DialogOption
			{
				text = text
			};
			currentPage.options.Add(option);
		}
		return option;
	}

	private DialogInitialPage FindOrAddInitialPage(DialogPage page)
	{
		DialogInitialPage initialPage = tree.initialPages.FirstOrDefault(p => p.page == page);
		if (initialPage == null)
		{
			initialPage = new DialogInitialPage
			{
				page = page
			};
			tree.initialPages.Add(initialPage);
		}
		return initialPage;
	}

	public DialogTreeBuilder InitialPage(string pageName)
	{
		DialogPage page = FindOrAddPage(pageName);
		DialogInitialPage initialPage = FindOrAddInitialPage(page);
		currentInitialPage = initialPage;
		currentConditionOwner = initialPage;
		return this;
	}

	public DialogTreeBuilder Page(string pageName)
	{
		DialogPage page = FindOrAddPage(pageName);
		currentPage = page;
		currentReply = null;
		currentOption = null;
		currentConditionOwner = null;

		return this;
	}

	public DialogTreeBuilder Reply(string text)
	{
		if (currentPage == null)
		{
			throw new System.Exception("No page specified");
		}
		DialogReply reply = FindOrAddReply(text);
		currentReply = reply;
		currentConditionOwner = reply;
		return this;
	}

	public DialogTreeBuilder Option(string text)
	{
		if (currentPage == null)
		{
			throw new System.Exception("No page specified");
		}
		DialogOption option = FindOrAddOption(text);
		currentOption = option;
		currentConditionOwner = option;
		return this;
	}

	public DialogTreeBuilder OptionsFrom(string pageName)
	{
		if (currentPage == null)
		{
			throw new System.Exception("No page specified");
		}
		DialogPage page = FindOrAddPage(pageName);
		currentPage.optionsFrom.Add(page);
		return this;
	}

	public DialogTreeBuilder Next(string pageName)
	{
		if (currentOption == null)
		{
			throw new System.Exception("No option specified");
		}
		DialogPage page = FindOrAddPage(pageName);
		currentOption.next = page;
		return this;
	}

	public DialogTreeBuilder Condition(System.Func<bool> func)
	{
		if (currentConditionOwner == null)
		{
			throw new System.Exception("No condition owner specified");
		}
		currentConditionOwner.conditions.Add(func);
		return this;
	}

	public DialogTreeBuilder Action(System.Action func)
	{
		if (currentOption == null)
		{
			throw new System.Exception("No option specified");
		}
		currentOption.actions.Add(func);
		return this;
	}

	public DialogTreeBuilder RandomReply()
	{
		if (currentPage == null)
		{
			throw new System.Exception("No page specified");
		}
		currentPage.randomReply = true;
		return this;
	}

	public DialogTreeBuilder End()
	{
		return this;
	}

	#region Helpers
	public DialogTreeBuilder Page(string pageName, string reply)
	{
		Page(pageName);
		Reply(reply);
		return this;
	}

	public DialogTreeBuilder SimplePage(string reply)
	{
		Page(reply);
		Reply(reply);
		return this;
	}

	public DialogTreeBuilder Inline(string text, string nextPageReply)
	{
		Option(text);
		var option = currentOption;
		Next(nextPageReply);
		Page(nextPageReply);
		Reply(nextPageReply);
		currentOption = option;
		currentConditionOwner = option;
		return this;
	}
	#endregion
}
