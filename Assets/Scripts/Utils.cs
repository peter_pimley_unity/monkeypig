﻿using System;

public static class Extensions
{

    public static T Next<T>(this T src) where T : struct
    {
        if (!typeof(T).IsEnum) throw new ArgumentException(string.Format("Argumnent {0} is not an Enum", typeof(T).FullName));

        var enumValues = (T[])Enum.GetValues(src.GetType());
        int j = Array.IndexOf(enumValues, src) + 1;
        return (enumValues.Length == j) ? enumValues[0] : enumValues[j];
    }

    public static InventoryManager.ItemTypes ConvertToInventoryType(this ProgressionSystem.RocketPart _rocketPart)
    {
        switch (_rocketPart)
        {
            case ProgressionSystem.RocketPart.Bottom:
                return InventoryManager.ItemTypes.RocketPart1;
            case ProgressionSystem.RocketPart.Mid:
                return InventoryManager.ItemTypes.RocketPart2;
            case ProgressionSystem.RocketPart.Top:
                return InventoryManager.ItemTypes.RocketPart3;
            default:
                throw new ArgumentOutOfRangeException(nameof(_rocketPart), _rocketPart, null);
        }
    }
}